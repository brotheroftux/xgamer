#include "clientclass.h"
#include <QDebug>
#include "mainwindow.h"

ClientClass::ClientClass(const QString& host, int port, QObject *parent):
    QObject(parent)
{
    sock = new QTcpSocket(this);
    sock->connectToHost(host, port);
    connect(sock, SIGNAL(connected()), this, SLOT(slotConnected()));
    connect(sock, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));
    connect(sock, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slotError(QAbstractSocket::SocketError)));
}
void ClientClass::slotConnected(){
    qDebug() << "Connected to server";
}
void ClientClass::slotError(QAbstractSocket::SocketError err){
    qDebug() << err;
}
void ClientClass::slotReadyRead(){
    while(sock->canReadLine()){
        QString answ = QString::fromUtf8(sock->readLine().trimmed());
        lastRead = answ;
        //connect(this, SIGNAL(sendDataToMW(QString)), mwind, SLOT(parseData(QString)));
        emit canRead();
    }
}
void ClientClass::send(QString text){
    sock->write(text.toUtf8());
}
QString ClientClass::getLastRead(){
    return lastRead;
}
