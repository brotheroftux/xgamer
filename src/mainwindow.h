#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QModelIndex>
#include <QListWidgetItem>
#include "clientclass.h"



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    /* void on_btnShop_clicked();

    * void on_btnGames_clicked();

    * void on_btnFriendlist_clicked();

   You: One does not simply delete this outdated stuff? IDE: I am too stupid!

   */

    void setAccessToken(QString par1);

    void refreshGames();
    void getReplyFinished();
    void readyReadReply();
    void fillList(QString par);
    void on_listWidget_itemClicked(QListWidgetItem *item);
    int getGameInfo(const QString& text);
    void readReplyInfo();
    void parseData(const QString &data);
    void read();


private:
    Ui::MainWindow *ui;
   // ClientClass *ccl;
    QString lastRead;
};

#endif // MAINWINDOW_H
