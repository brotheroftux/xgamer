#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QMainWindow>
#include "mainwindow.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>

namespace Ui {
class LoginWindow;
}

class LoginWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit LoginWindow(QWidget *parent = 0);
    ~LoginWindow();

signals:
    void sendToken(QString str);
    
private slots:
    void on_btnLogin_clicked();
    void getReplyFinished();
    void readyReadReply();
    void slotError();

private:
    Ui::LoginWindow *ui;
    MainWindow *wind;
};

#endif // LOGINWINDOW_H
