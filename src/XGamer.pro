#-------------------------------------------------
#
# Project created by QtCreator 2013-02-04T19:56:45
#
#-------------------------------------------------

QT       += core gui webkit network

TARGET = XGamer
TEMPLATE = app


SOURCES += main.cpp\
        loginwindow.cpp \
    mainwindow.cpp \
    clientclass.cpp

HEADERS  += \
    mainwindow.h \
    loginwindow.h \
    clientclass.h

FORMS    += loginwindow.ui \
    mainwindow.ui
