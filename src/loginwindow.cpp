#include "loginwindow.h"
#include "ui_loginwindow.h"
#include "mainwindow.h"
#include "QtNetwork/QtNetwork"
#include <QDebug>

QString accessToken;
QNetworkAccessManager manager;
QNetworkReply *reply;
QByteArray requestString;

LoginWindow::LoginWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LoginWindow)
{
    ui->setupUi(this);


}

LoginWindow::~LoginWindow()
{
    delete ui;
}

void LoginWindow::on_btnLogin_clicked()
{
    ui->textEmail->setEnabled(false);
    ui->textPass->setEnabled(false);
    ui->btnLogin->setEnabled(false);
    ui->btnRegister->setEnabled(false);
    QString strReq = QString("http://127.0.0.1/login.php") + "?login=" + ui->textEmail->text() + "&pass=" + ui->textPass->text();
    QUrl urlReq = strReq;
    QNetworkRequest request(urlReq);
    reply = manager.get(request);
    connect(reply, SIGNAL(finished()),this, SLOT(getReplyFinished()));
    connect(reply, SIGNAL(readyRead()), this, SLOT(readyReadReply()));
    //connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(slotError(QNetworkReply::NetworkError)));
}
void LoginWindow::getReplyFinished(){
    reply->deleteLater();
}
void LoginWindow::readyReadReply(){
    QString answer = QString::fromUtf8(reply->readAll());
    if (answer != "error" && answer != ""){
            accessToken = answer;
            wind = new MainWindow();
            connect(this, SIGNAL(sendToken(QString)), wind, SLOT(setAccessToken(QString))); // DAMN I LOVE SLOTS
            emit sendToken(accessToken);
    ui->labelStatus->setText(answer);

            wind->show();
            this->hide();
    }else{
        ui->labelStatus->setText("Invalid password or email");
        ui->textEmail->setEnabled(true);
        ui->textPass->setEnabled(true);
        ui->btnLogin->setEnabled(true);
        ui->btnRegister->setEnabled(true);
    }
}
void LoginWindow::slotError(){
    ui->labelStatus->setText("An error occured.");
}
