#ifndef CLIENTCLASS_H
#define CLIENTCLASS_H

#include <QObject>
#include <QTcpSocket>
#include "mainwindow.h"

class ClientClass : public QObject
{
    Q_OBJECT
private:
    QTcpSocket *sock;
    quint16 blockSize();
    QString lastRead;
signals:
    void sendDataToMW(const QString &data);
    void canRead();
private slots:
    void slotConnected();
    void slotReadyRead();
    void slotError(QAbstractSocket::SocketError err);
public:
    explicit ClientClass(const QString& host, int port, QObject *parent = 0);
    
signals:
    
public slots:
    void send(QString text);
    QString getLastRead();

};

#endif // CLIENTCLASS_H
