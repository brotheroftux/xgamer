/* main.cpp
* XGamer project main file
* Copyright brotheroftux et al, 2013
* License GPL v3
* http://brotheroftux.org/proj/xgamer
*/
#include <QtGui/QApplication>
#include "loginwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    LoginWindow w;
    w.show();
    
    return a.exec();
}
