/* mainwindow.cpp
* XGamer main window
* Copyright brotheroftux et al, 2013
* License GPL v3
* http://brotheroftux.org/proj/xgamer
*/
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QtNetwork/QtNetwork"
#include "QDebug"
#include <QModelIndex>
#include "clientclass.h"

QString token;
QNetworkAccessManager netmanager;
QNetworkReply *netreply;
QListWidgetItem *item = 0;
ClientClass *ccl;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //ui->btnShop->setChecked(true);
    ui->stkContent->setCurrentIndex(0);
    QUrl home("about:blank");
    ui->wvStore->setUrl(home);
    ui->labelAcc_Name->setText(token);
    ccl = new ClientClass("localhost", 2465);
    connect(ccl, SIGNAL(canRead()), this, SLOT(read()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

//void MainWindow::on_btnShop_clicked()
//{
//    ui->btnGames->setChecked(false);
//    ui->btnShop->setChecked(true);
//    ui->stkContent->setCurrentIndex(0);
//    QUrl home("http://127.0.0.1");
//    ui->wvStore->setUrl(home); // JUST FOR TESTING!!!
//    // TODO: Add here shop page (http://xgamer.brotheroftux.org/store) loading stuff.
//}

//void MainWindow::on_btnGames_clicked()
//{
//    ui->btnShop->setChecked(false);
//    ui->btnGames->setChecked(true);
//    ui->stkContent->setCurrentIndex(1);
//    // TODO: Add here user's library loading stuff.
//    refreshGames();
//}

//void MainWindow::on_btnFriendlist_clicked()
//{
//    friends = new FriendlistWindow();
//    friends->show();
//}
void MainWindow::setAccessToken(QString par1){
    token = par1;
    ui->labelAcc_Name->setText("<h1>Account " + token + "</h1>");
    refreshGames();
}
void MainWindow::refreshGames(){
    QString strReq = QString("http://localhost/games.php?nick=" + token);
    qDebug() << strReq;
    QUrl urlReq = strReq;
    QNetworkRequest request(urlReq);
    netreply = netmanager.get(request);
    connect(netreply, SIGNAL(finished()), this, SLOT(getReplyFinished()));
    connect(netreply, SIGNAL(readyRead()), this, SLOT(readyReadReply()));
}
void MainWindow::getReplyFinished(){
    netreply->deleteLater();
}
void MainWindow::readyReadReply(){
    QString answer = QString::fromUtf8(netreply->readAll());
    qDebug() << answer;
    if(answer != "error"){
        fillList(answer);
    }
}
void MainWindow::fillList(QString par){
    ui->listWidget->clear();
    QStringList lst;
    lst = par.split(":");
    qDebug() << lst;
    item = new QListWidgetItem("Home", ui->listWidget);
    item = new QListWidgetItem("Store", ui->listWidget);
    foreach(QString str, lst) {
        item = new QListWidgetItem(str, ui->listWidget);

        //item->setIcon(QPixmap("games/" + str + "/icon.png")); Unfortunately, there are no icons now
    }
}



void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
    ui->stkContent->setCurrentIndex(item->text() == "Home" ? 0 : item->text() == "Store" ? 1 : getGameInfo(item->text())); // TODO: primary-expression = int getGameInfo()
}
int MainWindow::getGameInfo(const QString &text){
    ui->labelName->setText("<h1>" + text + "</h1>");
    ui->labelDesc->setText("Loading..");
    QString strReq = QString("http://localhost/gameinfo.php?name=" + text);
    QUrl urlReq = strReq;
    QNetworkRequest request(urlReq);
    netreply = netmanager.get(request);
    connect(netreply, SIGNAL(finished()), this, SLOT(getReplyFinished()));
    connect(netreply, SIGNAL(readyRead()), this, SLOT(readReplyInfo()));
    // TODO: add here SIGNAL(error()) implementation
    return 2;
}
void MainWindow::readReplyInfo(){
    QString answ = QString::fromUtf8(netreply->readAll());
    if (answ != "error" && answ != ""){
        ui->labelDesc->setText(answ);
    }
}
void MainWindow::parseData(const QString& data){
    //TODO: implement something here
}
void MainWindow::read(){
    lastRead = ccl->getLastRead();
}
